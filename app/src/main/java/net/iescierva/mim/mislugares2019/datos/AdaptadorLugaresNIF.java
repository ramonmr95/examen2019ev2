/*
 * Copyright (c) 2020. Miguel Angel
 */

package net.iescierva.mim.mislugares2019.datos;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.iescierva.mim.mislugares2019.R;
import net.iescierva.mim.mislugares2019.modelo.Lugar;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class AdaptadorLugaresNIF extends RecyclerView.Adapter<AdaptadorLugaresNIF.ViewHolder> {

    protected LugaresBD lugares; // Lista de lugares a mostrar
    protected LayoutInflater inflador;   //Crea Layouts a partir del XML
    protected Context contexto;          //Lo necesitamos para el inflador

    public AdaptadorLugaresNIF(LugaresBD lugares) {
        this.lugares = lugares;
    }

    public AdaptadorLugaresNIF(Context contexto, LugaresBD lugares) {
        this.contexto = contexto;
        this.lugares = lugares;
        inflador = (LayoutInflater) contexto
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.elemento_lista_nif, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Lugar lugar = lugares.elemento(position);
        holder.personaliza(lugar);
    }

    @Override
    public int getItemCount() {
        return lugares.size();
    }

    //Creamos nuestro ViewHolder, con los tipos de elementos a modificar
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView nombre, nif;

        public ViewHolder(View itemView) {
            super(itemView);
            nombre = itemView.findViewById(R.id.nombre);
            nif = itemView.findViewById(R.id.nombre_nif);
        }

        // Personalizamos un ViewHolder a partir de un lugar
        public void personaliza(Lugar lugar) {
            nombre.setText(lugar.getNombre());
            nif.setText(lugar.getNIF());
        }

    }


}
