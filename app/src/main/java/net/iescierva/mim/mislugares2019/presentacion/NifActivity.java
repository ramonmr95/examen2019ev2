/*
 * Copyright (c) 2020. Miguel Angel
 */

package net.iescierva.mim.mislugares2019.presentacion;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import net.iescierva.mim.mislugares2019.Aplicacion;
import net.iescierva.mim.mislugares2019.R;
import net.iescierva.mim.mislugares2019.datos.AdaptadorLugares;
import net.iescierva.mim.mislugares2019.datos.AdaptadorLugaresNIF;
import net.iescierva.mim.mislugares2019.datos.LugaresBD;

public class NifActivity extends AppCompatActivity {

    private AdaptadorLugaresNIF adaptadorLugaresNIF;
    private LugaresBD lugares;
    private String nombre;
    private String NIF;

    private RecyclerView recyclerView;
    public AdaptadorLugares adaptador;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.ItemDecoration separador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        lugares = ((Aplicacion)getApplication()).getLugares();
        adaptadorLugaresNIF = ((Aplicacion) getApplication()).adaptadorLugaresNIF;
        setContentView(R.layout.activity_nif);

        recyclerView = findViewById(R.id.recycler_view);

        //Asignarle el adaptador
        recyclerView.setAdapter(adaptadorLugaresNIF);

        //Asignar el LayoutManager, en este caso de tipo Linear
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        //asignarle el separador de elementos
        separador = new DividerItemDecoration(this,DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(separador);

    }
}
